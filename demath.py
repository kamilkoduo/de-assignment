import math


def G(x, n=100):
    mul = 1
    div = 1
    xp = x
    summ = xp
    for k in range(1, n + 1):
        mul *= -1
        mul /= k
        div += 2
        xp *= (x * x)
        summ += (mul * xp / div)
    return summ


def f(x, y):
    return 2 * y * x + 5 - x * x


def exact(xs, x0, y0):
    C = (y0 - x0 / 2) / math.exp(x0 ** 2) - 9 / 2 * G(x0)
    return xs, [(9 / 2 * G(x) + C) * math.exp(x ** 2) + x / 2 for x in xs]


def euler(n, f, x0, y0, X):
    h = (X - x0) / n
    x = [x0 + h * k for k in range(0, n + 1)]
    u = [y0] + [0] * n
    d = [0] * n
    for i in range(0, n):
        d[i] = (h * f(x[i], u[i]))
        u[i + 1] = (u[i] + d[i])
    return x, u


def improved_euler(n, f, x0, y0, X):
    h = (X - x0) / n
    x = [x0 + h * k for k in range(0, n + 1)]
    u = [y0] + [0] * n
    for i in range(0, n):
        m1 = f(x[i], u[i])
        m2 = f(x[i + 1], u[i] + h * m1)
        u[i + 1] = (u[i] + h * (m1 + m2) / 2)
    return x, u


def runge_kutta(n, f, x0, y0, X):
    h = (X - x0) / n
    x = [x0 + h * k for k in range(0, n + 1)]
    u = [y0] + [0] * n
    for i in range(0, n):
        k1 = f(x[i], u[i])
        k2 = f(x[i] + h / 2, u[i] + h / 2 * k1)
        k3 = f(x[i] + h / 2, u[i] + h / 2 * k2)
        k4 = f(x[i] + h, u[i] + h * k3)

        u[i + 1] = (u[i] + h / 6 * (k1 + 2 * k2 + 2 * k3 + k4))
    return x, u


def loc_er(exact, numerical, n, f, x0, y0, X):
    x, u = numerical(n, f, x0, y0, X)
    x, ex = exact(x, x0, y0)
    er = [0] * len(u)
    for i in range(len(u)):
        er[i] = (abs(u[i] - ex[i]))
    return x, er


def global_er(exact, numerical, n, f, x0, y0, X):
    x, er = loc_er(exact, numerical, n, f, x0, y0, X)
    return max(er)
