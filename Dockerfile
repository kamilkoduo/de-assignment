FROM jupyter/minimal-notebook

EXPOSE 8888

RUN pip install seaborn numpy ipywidgets pandas
