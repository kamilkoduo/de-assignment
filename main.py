import ipywidgets as widgets

from deplot import *

n = widgets.IntSlider(
    value=10,
    min=1,
    max=1000,
    step=1,
    description='Steps:',
    disabled=False,
    continuous_update=False,
    orientation='horizontal',
    readout=True,
    readout_format='d'
)
x0 = widgets.FloatText(
    value=0,
    description='X0:',
    disabled=False
)

y0 = widgets.FloatText(
    value=1,
    description='Y0:',
    disabled=False
)

X = widgets.FloatText(
    value=3,
    description='X:',
    disabled=False
)

widgets.interactive(update_plot, n=n, x0=x0, y0=y0, X=X)
