import matplotlib.pyplot as plt
import seaborn as sns

from demath import *


def update_sol_plot(ax, n, x0, y0, X):
    ax.clear()

    x, u = euler(n, f, x0, y0, X)
    ax.plot(x, u, label="Euler method")
    x, u = improved_euler(n, f, x0, y0, X)
    ax.plot(x, u, label="Improved Euler method")
    x, u = runge_kutta(n, f, x0, y0, X)
    ax.plot(x, u, label="Runge-Kutta method")
    x, u = exact(x, x0, y0)
    ax.plot(x, u, label="Exact solution")

    ax.set_xlabel("X")
    ax.set_ylabel("Y")
    ax.set_title("EXACT and NUMERICAL solutions graph\n")
    ax.legend()


def update_error_plot(ax, n, x0, y0, X):
    ax.clear()

    x, u = loc_er(exact, euler, n=n, f=f, x0=x0, y0=y0, X=X)
    ax.plot(x, u, label="Euler method error")

    x, u = loc_er(exact, improved_euler, n=n, f=f, x0=x0, y0=y0, X=X)
    ax.plot(x, u, label="Improved Euler method error")

    x, u = loc_er(exact, runge_kutta, n=n, f=f, x0=x0, y0=y0, X=X)
    ax.plot(x, u, label="Runge-Kutta method error")

    ax.set_xlabel("X")
    ax.set_ylabel("Y")
    ax.set_title("Local truncation errors graph\n")
    ax.legend()


def update_gte_plot(ax, N, x0, y0, X):
    ax.clear()
    indexes = range(1, N+1)
    ax.plot(indexes,
            [global_er(exact, euler, n=x, f=f, x0=x0, y0=y0, X=X) for x in indexes],
            label="Euler method GTE")
    ax.plot(indexes,
            [global_er(exact, improved_euler, n=x, f=f, x0=x0, y0=y0, X=X) for x in indexes],
            label="Improved Euler method GTE")
    ax.plot(indexes,
            [global_er(exact, runge_kutta, n=x, f=f, x0=x0, y0=y0, X=X) for x in indexes],
            label="Runge-Kutta method GTE")

    ax.set_xlabel("N")
    ax.set_ylabel("Error")
    ax.set_title("Global truncation errors graph")
    ax.legend()


def update_plot(n=8, x0=0, y0=1, X=3):
    sns.set()

    fig, ax_sol = plt.subplots(figsize=(20, 10))
    update_sol_plot(ax_sol, n, x0, y0, X)

    fig, ax_er = plt.subplots(figsize=(20, 10))
    update_error_plot(ax_er, n, x0, y0, X)

    fig, ax_gte = plt.subplots(figsize=(20, 10))
    update_gte_plot(ax_gte, 100, x0, y0, X)

    plt.show()
